# Purple Startpage
Basic startpage. Inspired by various sources from [r/startpage](https://www.reddit.com/r/startpages/). Made with Bootstrap.  

### Weather

wttr.in API is used for weather information. For more details: [wttr](https://github.com/chubin/wttr.in)

To get the weather information for your city, simply replace the parameter in the `getWeatherData()` function on line 13 of the `index.html` with the city you want.

Example: `getWeatherData('Clausthal')` => `getWeatherData('New York')`

### Screenshot

![Screenshot 2023-01-02 at 10-40-46 Startpage](https://user-images.githubusercontent.com/19970595/210214951-2b26faab-7a68-4de1-8ae8-363984fad764.png)
